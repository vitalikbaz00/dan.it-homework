export const modalParams = [
    {
        id: 0,
        title: 'Once you delete this file, it wont be possible to undo this action. Are you sure you want to delete it?',
        headerTitle: 'Do you want to delete this file?',
        firstBtnText: 'Ok',
        secondBtnText: 'Cancel',

    },

    {
        id: 1,
        title: 'Please give Us your opinion about this modal. Is it OK? ',
        headerTitle: 'Do you like this modal?',
        firstBtnText: 'Yes',
        secondBtnText: 'No',

    }
]

