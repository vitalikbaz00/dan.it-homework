import React, {Component} from 'react';
import './Button.scss'


class Button extends Component {


    handleClick = () => {
        this.props.onClick()
    }

    render() {
        const {text, backgroundColor, onClick} = this.props
        return (
            <div className='btn__wrapper'>
                <button className='btn' onClick={this.handleClick}
                        style={{backgroundColor: backgroundColor}}>{text}</button>
            </div>
        )
    }
}

Button.defaultProps = {
    text: 'button',
    onClick: () => {
    },
    backgroundColor: 'green'
}

export default Button