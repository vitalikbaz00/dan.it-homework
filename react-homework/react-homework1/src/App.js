import React from "react";
import './App.css';
import Button from './components/Button/Button'
import './components/Button/Button.scss'
import Modal from "./components/Modal/Modals";
import {modalParams} from "./components/Modal/modalParams";

class App extends React.Component {
    state = {
        modalId: null
    }

    openModal(id) {
        this.setState({modalId: id})
    }

    closeModal() {
        this.setState({modalId: null})
    }


    render() {
        const {modalId} = this.state
        //find modal by id and check for equal with state
        const modal = modalParams.find(item => item.id === modalId)
        return (
            <>
                <div>
                    <Button text='Open first modal' backgroundColor='red' onClick={() => this.openModal(0)}/>
                    <Button text='Open second modal' backgroundColor='blue' onClick={() => this.openModal(1)}/>
                </div>
                {modal && <Modal
                    //never use id(index) for giving key
                    onClick={() => this.closeModal()}
                    key={modal.id}
                    header={modal.headerTitle}
                    text={modal.title}
                    firstBtnText={modal.firstBtnText}
                    secondBtnText={modal.secondBtnText}
                />}
            </>


        )
    }
}

export default App
