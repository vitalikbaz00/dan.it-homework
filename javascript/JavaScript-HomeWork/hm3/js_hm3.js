// Задание
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Считать с помощью модального окна браузера два числа.
//     Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
//     Вывести в консоль результат выполнения функции.
//
//
//     Необязательное задание продвинутой сложности:
//
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел числа,
//     либо при вводе указал не числа, - спросить оба числа заново (при этом значением по удмолчанию для каждой из переменных должна быть введенная ранее информация).

let firstNumber = +prompt('Type first number')
let secondNumber = +prompt('Type second number')

while (firstNumber === '' || isNaN(firstNumber) || secondNumber === '' || isNaN(secondNumber)){
    firstNumber = prompt('Type first number');
    secondNumber = prompt('Type second number')
}

let operator = prompt('Type a math sign')
while (operator === '' && operator !== '/' && operator !== '*'&& operator !== '-'&& operator !== '+'){
    operator = prompt('Type a math sign')
}

const calculateNumbers = (num1, num2, operator) => {
    switch (operator) {
        case '+':
            console.log(`Addition result ${num1+num2}`)
            break
        case '-':
            console.log(`Subtraction result ${num1-num2}`)
            break
        case '*':
            console.log(`Multiply result ${num1*num2}`)
            break
        case '/':
            console.log(`Division result ${num1+num2}`)
            break
    }
}
calculateNumbers(firstNumber, secondNumber, operator)