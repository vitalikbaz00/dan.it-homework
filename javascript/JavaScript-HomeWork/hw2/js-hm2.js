let userNumber = +prompt('Please type a number')

while (isNaN(userNumber)){
    userNumber = +prompt('Please type only a number')
    if (userNumber){
        break
    }
}
for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0){
        console.log(i)
    }
}

///Циклы нужны для сокращения кода, так как нам иногда нужно повторять одинаковые действия много раз.
// (если условие остается true). Циклы можно помещать внутрь друг друга. Цикл работает по инструкции, которую мы ему задаем.

