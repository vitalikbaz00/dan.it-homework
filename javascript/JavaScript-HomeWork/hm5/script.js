// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
//     соединенную с фамилией (в нижнем регистре) и годом рождения.
//     (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser(firstName, secondName) {
    let newUser = {
        name: firstName,
        lastName: secondName,
        getLogin: function () {
            let firstLetter = firstName.charAt(0)
            let login = firstLetter + secondName
            return login.toLowerCase()
        },
        getAge: function (userAge){
            let ageArray = userAge.split('.')
            let birthday = new Date(`${ageArray[2]},${ageArray[1]},${ageArray[0]} `)
            let dif = new Date() - birthday
            let difDays = dif / (60 * 60 * 24 ) / 1000
            let age = Math.floor(difDays / 365.25)
            return age

        },
        getPassword: function (firstName, secondName, age){
            let upperFirstLetter = firstName.charAt(0).toUpperCase()
            let lowerSecondName = secondName.toLowerCase()
            let birthdayDateSplit = age.split('.')
            let birthdayYear = birthdayDateSplit[2]
            let password =  upperFirstLetter + lowerSecondName + birthdayYear
            return password

        }

    }
    return newUser
}


let firstName = prompt('Enter your name')
let secondName = prompt('Enter your last name')
let userAge = prompt('Enter your age','dd.mm.yyyy')


const user = createNewUser(firstName, secondName)

console.log(user.getAge(userAge))
console.log(user.getPassword(firstName, secondName, userAge))

