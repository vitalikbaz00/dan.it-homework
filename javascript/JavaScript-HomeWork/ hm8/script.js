// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.
//

const elements = [
 {
  tag: 'span',
  classes: 'span',
  type: 'number',
  textContent: '',

 },
 {
  tag: 'button',
  classes: 'xButton',
  textContent: 'X',
 }
]

function createHTMLElement(array, valueInput) {

 const spanWrapper = document.createElement("div")
 array.map((object => {

  let createElem = document.createElement(`${object.tag}`);
  createElem.className = object.classes;
  createElem.textContent = object.textContent
  if (object.tag === "span") {
   createElem.textContent = `Текущая цена : ${valueInput}`;
  }
  spanWrapper.append(createElem)

 }))
 wrapper.append(spanWrapper)

}

//Get elements
let wrapper = document.querySelector(".wrapper");
let input = document.querySelector(".input");

input.addEventListener("focus", e =>{
     e.target.className = "input-green"
    }
)

input.addEventListener("blur", e => {
 // debugger
 const inputVal = e.target.value;
 if(e.target.value === ""){
  e.target.className = "input";
 }
 const error = document.querySelector('.error')
 if (error) error.remove();
 if (inputVal && inputVal >= 0) {
  e.target.classList.add('input-green');
  createHTMLElement(elements, inputVal)
  e.target.value = ''

 }
 if(inputVal < 0) {
  e.target.className = 'input-red'
  wrapper.insertAdjacentHTML("beforebegin", `<span class="error">Please enter correct price</span>`)
  e.target.value = ''
 }
})

wrapper.addEventListener('click', e => {
 if (e.target.className.includes("xButton")) {
  input.value = "";
  e.target.parentElement.remove()
 }
})
