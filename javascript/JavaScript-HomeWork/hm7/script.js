let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]

let listElement = document.createElement('div')

function showArr(arr, element) {
    element.insertAdjacentHTML("afterbegin", `<ul>${arr.map((element) => {
        return `<li>${element}</li>`
    }).join(' ')}</ul>`)
    document.body.append(element)
}
showArr(arr, listElement)




