// Получить от пользователя 2 числа. Спрашивать его
// число заново до тех пор, пока он не введет валидное число.
// Проверку на валидность - оформить в функцию, которой мы передаем значание, а
// функция проверяет значение на null, на пустую строку, на NaN и возвращает true
// если число валидное, иначе false;
// Если 2 числа валидные, вывести в консоль и на экран - результат их сложения
let number1 = prompt("please type first number")
let number2 = prompt("please type second number")

function isValidNumber(value) {
    if (value === '' || value === null || Number.isNaN(+value)){
       return false;
    }
    return true;
}
while (!isValidNumber(number1)){
    prompt("please type first number")
}
while (!isValidNumber(number2)){
    prompt("please type second number")
}

console.log(+number1+ +number2)