// /**
//  * Задание
//  *
//  * Написать программу, которая будет опрашивать и приветствовать пользователя.
//  *
//  * Программа должна узнать у пользователя его:
//  * - Имя;
//  * - Фамилию;
//  * - Год рождения.
//  *
//  * Если пользователь вводит некорректные данные,
//  * программа должна повторно опрашивать его до тех пор,
//  * пока данные не будут введены корректно.
//  *
//  * Данные считается введёнными некорректно, если:
//  * - Пользователь не вводит в поле никаких данных;
//  * - Год рождения, введённый пользователем меньше, чем 1910 или больше, чем текущий год.
//  *
//  * Когда пользователь введёт все необходимые данные корректно,
//  * вывести в консоль сообщение:
//  * «Добро пожаловать, родившийся в ГОД_РОЖДЕНИЯ, ИМЯ ФАМИЛИЯ.».
//  */

let userName = prompt('Please type your name');
while (userName === '' || userName === null) {
    userName = prompt('Please type your name ')
};

let userLastName = prompt('Please type your last name')
while (userLastName === '' || userLastName === null) {
    userLastName = prompt('Please type your last name ');
};

let userYearOfBirth = prompt('Please type your year of birth');
const minYear = 1910;
const currentYear = 2021;
while (userYearOfBirth === '' ||
userYearOfBirth === null ||
Number.isNaN(+userYearOfBirth ||
+userYearOfBirth < minYear ||
    +userYearOfBirth > currentYear)) {
    userYearOfBirth = prompt('Please type your year of birth  ')
}
console.log(`Добро пожаловать, родившийся в ${userYearOfBirth} , ${userName} ${userLastName}`)