// Написать программу, которая будет приветствовать пользователя.
// Сперва пользователь вводит своё имя, после чего программа выводит
// в консоль сообщение с учётом его должности.

// Например 'Добро пожаловать, CTO Mike'

//  Список должностей:
//   Mike — CEO;
//   Jane — CTO;
//   Walter — программист:
//  Oliver — менеджер;
//   John — уборщик.

//   Если введёно не известное программе имя — вывести в консоль сообщение «Пользователь не найден.».

//   Выполнить задачу в двух вариантах:
//   - используя конструкцию if/else if/else;
//   - используя конструкцию switch.

// 1 variant
// const userName = prompt("enter your name");
// if (userName === "Mike"){
//     console.log("Weclome,CEO Mike")
// }else if (userName === "Jane"){
//     console.log("Weclome,CTO Jane")
// }else if (userName === "Walter") {
//     console.log("Weclome,programist Walter")
// }else if (userName === "Oliver") {
//     console.log("Weclome,manager Oliver")
// }else if (userName === "John") {
//     console.log("Weclome,cleaner John")
// }else {
//     console.log("User not founded ")
// }


const userName = prompt("enter your name")
switch (userName) {
    case "Jane":
        console.log("Weclome,CEO Mike");

        break;
    case "Mike":
        console.log("Weclome,CTO Jane");

        break;
    case "Walter":
        console.log("Weclome,programist Walter");
        break;
    default:console.log("User not founded ")
        break;
}