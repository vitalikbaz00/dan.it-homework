// *
//  * Задание.
//  *
//  * Написать программу-помощник преподавателя.
//  *
//  * Будем использовать американскую систему оценивания знаний.
//  * Эта система работает на баллах и оценках в виде букв.
//  * Расшифровывается следующим образом:
//  *
//  * Баллы  | Оценка |
//  * 95-100 | A      |
//  * 90-94  | A-     |
//  * 85-89  | B+     |
//  * 80-84  | B      |
//  * 75-79  | B-     |
//  * 70-74  | C+     |
//  * 65-69  | C      |
//  * 60-64  | C-     |
//  * 55-59  | D+     |
//  * 50-54  | D      |
//  * 25-49  | E      |
//  * 0-24   | F      |
//  * -----------------
//  *
//  * Программа должна спрашивать имя и фамилию студента, а также количество баллов, которое он набрал.
//  *
//  *
//  *
//  * Если все данные данные введены верно, программа конвертирует
//  * числовое количество баллов в буквенную оценку и выводит сообщение в консоль:
//  * «К студенту ИМЯ_СТУДЕНТА прикреплена оценка «ОЦЕНКА».».
//  *
//  * После выведения этого сообщения программа должна спросить,
//  * есть-ли необходимость сконвертировать оценку для ещё одного студента,
//  * и должна начинать свою работу сначала до тех пор, пока пользователь не ответит «Нет.».
//  *
//  * Когда пользователь откажется продолжать работу программы, программа выводит сообщение:
//  * «✅ Работа завершена.».



do {
    let user = prompt('Please type your name and last name');
    let userMark = prompt('Please type your mark')


    while (user === '' || user === null){
        user = prompt('Please type your name and last name');
    }

    const maxMark = 100;
    const minMark = 0;
    let grade = null;

    while (userMark === '' ||
    userMark === null ||
    Number.isNaN(+userMark) ||
    userMark > maxMark ||
    userMark < minMark){
        userMark = prompt('Please type your mark');
    }

    if (userMark <= 24) {
        grade = 'F'
    }else if (userMark >= 25 && userMark <= 49) {
        grade = 'E'
    }else if (userMark >= 50 && userMark <= 54){
        grade = 'D'
    }else if (userMark >= 55 && userMark <= 59){
        grade = 'D+'
    }else if (userMark >= 60 && userMark <= 64){
        grade = 'C-'
    }else if (userMark >= 65 && userMark <= 69){
        grade = 'C'
    }else if (userMark >= 70 && userMark <= 74){
        grade = 'C+'
    }else if (userMark >= 75 && userMark <= 79){
        grade = 'B-'
    }else if (userMark >= 80 && userMark <= 84){
        grade = 'B'
    }else if (userMark >= 85 && userMark <= 89){
        grade = 'B+'
    }else if (userMark >= 90 && userMark <= 94){
        grade = 'A-'
    }else if (userMark >= 95 && userMark <= 100){
        grade = 'A'
    }

    console.log(`К студенту ${user} прикреплена оценка ${grade}`);


}
    while (confirm('Do you want to continue'))

alert('Job is done')