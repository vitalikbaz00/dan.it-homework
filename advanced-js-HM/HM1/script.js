class Employee {
    constructor(name,age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName(){
        return this.name
    };
    getAge(){
        return this.age
    };
    getSalary(){
        return this.salary
    };
}


class Programmer extends Employee{
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this.lang = lang;
    };
    getName() {
        return super.getName();
    };
    getAge() {
        return super.getAge();
    };

    getSalary() {
        return super.getSalary() * 3;
    };

}

const ilya = new Programmer('Ilya',21,2000,'Js');
console.log(ilya);
console.log(ilya.getSalary());

const sasha = new Programmer('Sasha',19,4690,'C++');
console.log(sasha);
console.log(sasha.getSalary());