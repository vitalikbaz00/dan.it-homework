// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price).
// Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
//     Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

    const booksArr = document.getElementById('root');

    books.forEach((el) => {const {author, name, price} = el;
    if (author && name && price){
        booksArr.insertAdjacentHTML('afterbegin',`<ul><li>${author}</li><li>${name}</li><li>${price}</li></ul>`);
    }
    })
function checkEl(el) {
    const {author, name, price} = el
        if (!author){
            throw new Error("author not entered")
        }
        if (!name){
            throw new Error("name not entered")
        }
        if (!price){
            throw new Error("price not entered")
        }
        return el
}
function checkBooks(arr){
        for (let i = 0; i < arr.length; i++)
        try {
            checkEl(arr[i])
        }
        catch(e){
            console.error(e)
        }
}
checkBooks(books)



