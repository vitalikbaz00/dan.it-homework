// let gulp = require('gulp') // Подключаем Gulp
// let sass = require('gulp-sass')(require('sass'));
// gulp.task('sass', function() { // Создаем таск "sass"
//     return gulp.src(['sass/**/*.sass', 'sass/**/*.scss']) // Берем источник
//         .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // Преобразуем Sass в CSS посредством gulp-sass
//         .pipe(gulp.dest('css')) // Выгружаем результата в папку css
// });
//
// gulp.task('watch', function() {
//     gulp.watch(['sass/**/*.sass', 'sass/**/*.scss'], ['sass']); // Наблюдение за sass файлами в папке sass
// });
//
// gulp.task('default', ['watch']);

// let project_folder = 'dist'
// let source_folder = 'src'
//
// let path = {
//     build: {
//         html: project_folder + '/',
//         css: project_folder + '/css/',
//         js: project_folder + '/js/',
//         img: project_folder + '/img/'
//     },
//     src: {
//         html: [source_folder + '/*.html', '!' + source_folder + '/_*.html'],
//         css: source_folder + '/scss/style.scss',
//         js: source_folder + '/js/script.js',
//         img: source_folder + '/img/**/*.{jpg,png,svg}'
//     },
//     watch: {
//         html: source_folder + '/**/*.html',
//         css: source_folder + '/scss/**/*.scss',
//         js: source_folder + '/js/**/*.js',
//         img: source_folder + '/img/**/*.{jpg,png,svg}'
//     },
//     clean: './' + project_folder + '/'
// }
//
// let {src, dest} = require('gulp'),
//     gulp = require('gulp')
// browsersync = require('browser-sync').create(),
//     fileinclude = require('gulp-file-include'),
//     del = require('del'),
//     scss = require('gulp-sass')(require('sass')),
//     autoprefixer = require('gulp-autoprefixer'),
//     group_media = require('gulp-group-css-media-queries'),
//     clean_css = require('gulp-clean-css'),
//     rename = require('gulp-rename'),
//     concat = require('gulp-concat');
//
//
// function browserSync() {
//     browsersync.init({
//         server: {
//             baseDir: './' + project_folder + '/'
//         },
//         port: 3004,
//         notify: false
//     })
// }
//
// function html() {
//     return src(path.src.html)
//         .pipe(fileinclude())
//         .pipe(dest(path.build.html))
//         .pipe(browsersync.stream())
// }
//
// function css() {
//     return src(path.src.css)
//         .pipe(
//             scss({
//                 outputStyle: 'expanded'
//             })
//         )
//         .pipe(
//             group_media()
//         )
//         .pipe(
//             autoprefixer({
//                 overrideBrowserslist: ['last 5 versions'],
//                 cascade: true
//             })
//         )
//         .pipe(dest(path.build.css))
//         .pipe(clean_css())
//         .pipe(
//             rename({
//                 extname: '.min.css'
//             })
//         )
//         .pipe(dest(path.build.css))
//         .pipe(browsersync.stream())
// }
//
// function watchFiles() {
//     gulp.watch([path.watch.html], html)
//     gulp.watch([path.watch.css], css)
// }
//
// function clean() {
//     return del(path.clean)
// }
//
// let build = gulp.series(clean, gulp.parallel(css, html));
// let watch = gulp.parallel(build, watchFiles, browserSync);
//
// exports.css = css;
// exports.html = html;
// exports.build = build;
// exports.watch = watch;
// exports.default = watch;
const gulp = require('gulp'),
    clean = require('gulp-clean'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    minifyjs = require('gulp-js-minify'),
    imagemin = require('gulp-imagemin'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass')(require('sass')),
    browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

const paths = {
    build: {
        css: './dist/css/',
        js: './dist/js/',
        img: './dist/img/',
        self: './dist/'
    },
    src: {
        scss: './src/scss/**/*.scss',
        js: './src/js/*.js',
        img: './src/img/**/*'
    }
}

const buildJS = () => (
    gulp.src(paths.src.js)
        .pipe(concat('store.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(minifyjs())
        .pipe(gulp.dest(paths.build.js))
        .pipe(browserSync.stream())
);

const buildIMG = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5,
            svgoPlugins: [
                {
                    removeViewBox: true
                }
            ]
        }))
        .pipe(gulp.dest(paths.build.img))
        .pipe(browserSync.stream())
);

const buildCSS = () => (
    gulp.src(paths.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compability: 'ie8'}))
        .pipe(autoprefixer({overrideBrowserlist: ["last 5 versions"]}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.build.css))
        .pipe(browserSync.stream())
);

const cleanBuild = () => (
    gulp.src(paths.build.self, {allowEmpty: true})
        .pipe(clean())
)

const watcher = () => {
    browserSync.init({
        server: {baseDir: './'}
    })
    gulp.watch(paths.src.scss, buildCSS).on('change', browserSync.reload)
    gulp.watch(paths.src.js, buildJS).on('change', browserSync.reload)
    gulp.watch(paths.src.img, buildIMG).on('change', browserSync.reload)
}

const build = gulp.series(
    buildCSS,
    buildJS
)

gulp.task('dev', watcher)
gulp.task('build', gulp.series(cleanBuild, build, buildIMG))