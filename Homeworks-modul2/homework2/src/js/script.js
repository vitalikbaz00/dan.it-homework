
const burgerMenu = document.querySelector(".burger-menu")
const burger = document.querySelector(".burger-btn");
document.addEventListener('click',  (e) =>
{
    const burgerBtn = e.target.closest(".burger-btn")
    if (burgerBtn){
        burgerMenu.classList.toggle('visible')
        burger.classList.toggle('is-active')
    }else {
        burgerMenu.classList.remove('visible')
        burger.classList.remove('is-active')
    }

})



