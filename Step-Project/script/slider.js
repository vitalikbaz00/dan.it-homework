$(function () {
    $('.slide-wrapper').slick({
        slidesToShow: 1,
        infinite: true,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor:'.slider-nav'
    })
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        centerMode: true,
        asNavFor:'.slide-wrapper'
    });
})
